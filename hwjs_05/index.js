//1. Разобраться, что выводит код и написать объяснение в комментарий

let myVar; // myVar = ubdefind 
myVar = 5; // myVar = 5
let myNum; // myVar = 5 myNum = undefind
myNum = myVar; // myVar = 5 myNum = 5
myVar = 10; // myVar = 10 myNum = 5
myNum = myVar; // myVar = 10 myNum = 10

//2. Разобраться, что выводит код и написать объяснение в комментарий

const myVar; //константа не инициализирована +ошибка
myVar = 5; // присвоение константе +ошибка
const myNum; //константа не инициализирована
myNum = myVar; // присвоение константе +ошибка
myVar = 10; // присвоение константе +ошибка
myNum = myVar; // присвоение константе +ошибка

//3. Разобраться, что выводит код и написать объяснение в комментарий

let a = 123 // a = 123
let b = -123; //a = 123, b = -123   
let c = "Hello";// a = 123, b = -123, c = "Hello"
const e = 123; // a = 123, b = -123, c = "Hello", e = 123

a = a + 1;  // a = 124, b = -123, c = "Hello", e = 123
b =+ 5; // a = 124, b = 5, c = "Hello", e = 123
c = c + " world!" // a = 124, b = 5, c = "Hello world!", e = 123
e = e + 123; // присвоение константе +ошибка

//4. Разобраться, что выводит код и написать объяснение в комментарий

let a = 0;
let b;

if (a) { // a = 0 => false
 console.log(a);
} else if (b) { // b = undefind => false
 console.log(b);
} else {
 console.log('hello world'); // попали сюда
}

//5. Разобраться, что выводит код и написать объяснение в комментарий

let a = "Hello world"; // строка
console.log(typeof a);
if  ((typeof a) === "string") { // "string" === "string"   
 console.log(a); // попадаем сюда,  выводим в консоль "Hello world"
}

//6. Разобраться, что выводит код и написать объяснение в комментарий

let a = {}; // создаем пустой объект
let b = a; // ссылаемся на тот же объект 

alert( a == b ); 
alert( a === b ); // обе переменные ссылается на один и тот же объект, будет true в обоих случаях
